module github.com/luisnquin/luisnquin/resume

go 1.20

require (
	github.com/go-cmd/cmd v1.4.1
	github.com/go-pdf/fpdf v0.6.0
	github.com/gookit/color v1.5.2
	github.com/mitchellh/go-ps v1.0.0
	github.com/rs/zerolog v1.29.0
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/xo/terminfo v0.0.0-20210125001918-ca9a967f8778 // indirect
	golang.org/x/sys v0.6.0 // indirect
)
